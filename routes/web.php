<?php
Auth::routes();
// please note: i isolate post method in another group because localization prevent post method
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale() . '/admin',
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath','auth']
    ]
    , function () {
    Route::get('/', function () {
        return view('admin.admin_home');
    });
    //category get routes
    Route::get('/category', 'CategoryController@index');
    Route::get('/category/create', 'CategoryController@create');

    Route::get('/category/edit/{id}', 'CategoryController@edit');

    Route::get('/category/delete/{id}', 'CategoryController@delete');

    //Post get routes
    Route::get('/post', 'PostController@index');
    Route::get('/post/create', 'PostController@create');

    Route::get('/post/edit/{id}', 'PostController@edit');
    Route::get('/post/details/{id}', 'PostController@details');
    Route::get('/post/delete/{id}', 'PostController@delete');


    //user get routes
    Route::get('/user', 'UserController@index');
    Route::get('/user/create', 'UserController@create');
    Route::get('/user/edit/{id}', 'UserController@edit');
    Route::post('/user/update/{id}', 'UserController@update');
    Route::get('/user/delete/{id}', 'UserController@delete');
});
//please note: i isolate post method in another group because localization prevent post method
Route::group(
    [
        'prefix' =>'admin',
        'middleware' => ['auth']
    ]
    , function () {
    //category post routes
    Route::post('/category/store', 'CategoryController@store');
    Route::post('/category/update/{id}', 'CategoryController@update');
    //Post post routes
    Route::post('/post/store', 'PostController@store');
    Route::post('/post/update/{id}', 'PostController@update');
    //user post routes
    Route::post('/user/store', 'UserController@store');
    Route::post('/user/update/{id}', 'UserController@update');
});

//client side routes
Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/client',
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
], function () {
    Route::get('/', 'ClientController@index');
    Route::get('/{slug}', 'ClientController@getCatNews');
    Route::get('/details/{id}', 'ClientController@details');
});

//logout
Route::get('/logout','Auth\LoginController@logout');



