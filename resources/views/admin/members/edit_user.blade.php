@extends('layouts.side')

<form class="form-horizontal" action="{{ url("/admin/user/update/$specific_user->id") }}" method="post" style="margin-top: 150px; margin-left: 50px;">

    {{ csrf_field() }}
    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="username">Enter admin name to update</label>
        <div class="col-md-5">
            <input id="username" name="name" type="text" value="{{ $specific_user->name }}" placeholder="" class="form-control input-md" required="">
        </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="email" value="{{ $specific_user->emial }}">Enter admin email to update</label>
        <div class="col-md-5">
            <input id="email" name="email" type="email" value="{{ $specific_user->email }}" placeholder="" class="form-control input-md" required="">
        </div>
    </div>

    <!-- Password input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="passwordinput">Enter admin password to update</label>
        <div class="col-md-5">
            <input id="passwordinput" name="passwordinput" type="password" placeholder="" class="form-control input-md">
        </div>
    </div>
    <!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="save"></label>
        <div class="col-md-4">

            <input type="submit" value="Update" name="save" class="btn btn-primary">
        </div>
    </div>


</form>

