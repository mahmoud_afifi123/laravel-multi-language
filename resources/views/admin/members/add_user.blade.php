@extends('layouts.side')
<form class="form-horizontal" action="{{ url('/admin/user/store') }}" method="post" style="margin-top: 150px;">
{{ csrf_field() }}
<!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="username">Enter new admin name</label>
        <div class="col-md-5">
            <input id="username" name="name" type="text" placeholder="" class="form-control input-md" required="">

        </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="email">Enter new admin email</label>
        <div class="col-md-5">
            <input id="email" name="email" type="email" placeholder="" class="form-control input-md" required="">

        </div>
    </div>
    <!-- Password input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="passwordinput">Enter new admin password</label>
        <div class="col-md-5">
            <input id="passwordinput" name="passwordinput" type="password" placeholder="" class="form-control input-md">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="save"></label>
        <div class="col-md-4">
            <input type="submit" value="save" name="save" class="btn btn-primary">
        </div>
    </div>
</form>
