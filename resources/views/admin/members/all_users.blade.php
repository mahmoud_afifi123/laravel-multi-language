@include('layouts.side')
<br>
<?php
$i = 1;
?>
<div class="container-fluid">
    @foreach($users as $user)
        <div style="margin-left: 300px;">
            <div class="row">
                <div class="col-md-4">
                    <h3>{{ $i .'-'.$user->name }}</h3>
                </div>
                <div class="col-md-4">
                    <h3>{{ $user->email }}</h3>
                </div>
                <div class="col-md-4">
                    <a href="{{ url("admin/user/delete/$user->id") }}" class="btn btn-danger">
                        Delete
                    </a>
                    <a href="{{ url("admin/user/edit/$user->id") }}" class="btn btn-primary">
                        Edit
                    </a>

                </div>
            </div>
        </div>

        <?php $i++;?>
    @endforeach
</div>
