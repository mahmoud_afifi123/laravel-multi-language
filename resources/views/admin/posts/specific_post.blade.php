@include('layouts.side')
<div class="container-fluid" style="margin-left: 220px; margin-top: 30px;">
    <div class="thumbnail">
        <img alt="Bootstrap Thumbnail Second" src="{{ url("/uploads/$specific_post->image") }}">
        <div class="caption">
            <h3>
                {{trans('post.title')}}:   {{ $specific_post->translate(LaravelLocalization::setLocale())->title}}
            </h3>
            <h3>
                <b>{{ $specific_post->translate(LaravelLocalization::setLocale())->description }}</b>
            </h3>
            <h3>
                {{trans('post.cat_slug')}}:       {{ $specific_post->categories->slug }}
            </h3>
            <h3>
                {{trans('post.cat_name')}}:       {{ $specific_post->categories->translate(LaravelLocalization::setLocale())->name }}
            </h3>
            <p>
                <a class="btn btn-primary"  href="{{ url("admin/post/details/$specific_post->id") }}">{{trans('post.more')}}</a>
                <a class="btn btn-danger"  href="{{ url("admin/post/delete/$specific_post->id") }}">{{trans('post.Delete')}}</a>
                <a class="btn btn-info" href="{{ url("admin/post/edit/$specific_post->id") }}">{{trans('post.Edit')}}</a>
				@if(LaravelLocalization::setLocale() == 'ar')
				<a class="btn btn-info" href="{{ url("en/admin/post/details/$specific_post->id") }}">In english</a>
				@else
					<a class="btn btn-info" href="{{ url("ar/admin/post/details/$specific_post->id") }}">للعربية</a>
				@endif
            </p>
        </div>
    </div>
</div>