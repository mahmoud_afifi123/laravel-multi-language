@include('layouts.side')
<div class="container-fluid" style="margin-left: 220px; margin-top: 30px;">
    <h1 align="center">{{trans('post.select')}}</h1>
    <h2 align="center"><a class="btn btn-primary" href="{{url('/en/admin/post')}}">English</a>    <a class="btn btn-primary"  href="{{url('/ar/admin/post')}}">العربية</a></h2>
    <hr>
	@if(count($all_posts)>0)
    @foreach($all_posts as $post)
        <div class="row">
            <div class="media @if(LaravelLocalization::setLocale() == 'ar') pull-right @endif">
                <a href="" class="@if(LaravelLocalization::setLocale() == 'ar') pull-right @else pull-left @endif"><img src="{{url("/uploads/$post->image")}}" width="200" class="media-object"></a>
                <div class="media-body">
                    <h4 class="media-heading">
                        {{trans('post.title')}}: {{ $post->translate(LaravelLocalization::setLocale())->title }}
                    </h4>
                    <h3>{{ substr($post->translate(LaravelLocalization::setLocale())->description,0,10) }}</h3>
                    <p>
                        <a class="btn btn-primary"  href="{{ url("admin/post/details/$post->id") }}">{{trans('post.more')}}</a>
                        <a class="btn btn-danger"  href="{{ url("admin/post/delete/$post->id") }}">{{trans('post.Delete')}}</a>
                        <a class="btn btn-info" href="{{ url("admin/post/edit/$post->id") }}">{{trans('post.Edit')}}</a>
                    </p>
                </div>
            </div>
        </div>
    @endforeach
	@else
	<h1 style="margin-left:400px; margin-top:120px;">{{trans('post.not_exist')}}</h1>	
@endif
</div>