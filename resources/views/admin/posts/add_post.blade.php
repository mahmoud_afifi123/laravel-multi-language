@include('layouts.side')
<link rel="stylesheet" type="text/css" href="{{ url('css/form.css') }}">
<form class="form-horizontal" method="post" action="{{url('/admin/post/store')}}" enctype="multipart/form-data" style="margin-left: 350px; margin-top: 100px;">
    {{ csrf_field() }}

    <div class="form-group">
        <label class="control-label col-sm-2" for="en_name">Select Post Category</label>
        <div class="col-sm-6">
            <select class="form-control" name="category_id" placeholder="" required="">
                @foreach($categories as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->slug }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="en_name">Enter post title in english</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="en_title" id="en_title" placeholder="" required="">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="en_name">ادخل عنوان المنشور بالعربية</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="ar_title" id="ar_title" placeholder="" required="">
        </div>
    </div>

    <div class="form-group">
        <label for="slug" class="control-label col-sm-2">Enter image</label>
        <div class="col-sm-6">
            <input type="file" name="image" id="image" placeholder="" required="">
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2" for="en_name">Enter post description in en</label>
        <div class="col-sm-6">
            <textarea class="form-control" id="en_description" name="en_description" rows="4" required=""></textarea>
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2" for="ar_name">ادخل وصف المنشور بالعربية</label>
        <div class="col-sm-6">
            <textarea class="form-control" id="ar_description" name="ar_description" rows="4" required=""></textarea>

        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for=""></label>
        <div class="text-left col-sm-10">
            <input type="submit" id="" value="Save" name="" class="btn btn-primary">

        </div>
    </div>
</form>
