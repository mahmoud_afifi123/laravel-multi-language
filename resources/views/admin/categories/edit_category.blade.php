@include('layouts.side')
<link rel="stylesheet" type="text/css" href="{{ url('css/form.css') }}">
<form class="form-horizontal" method="post" action="{{url("/admin/category/update/$specific_cat->id")}}" style="margin-left: 350px; margin-top: 100px;">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="slug" class="control-label col-sm-2">Enter slug</label>
        <div class="col-sm-6">
            <input type="text" value="{{ $specific_cat->slug }}" class="form-control" name="slug" id="slug" placeholder="">
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2" for="en_name">Enter category name in en</label>
        <div class="col-sm-6">
            <textarea class="form-control"  id="en_name" name="en_name" rows="4">{{$specific_cat->translate('en')->name}}</textarea>
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2" for="ar_name">ادخل اسم القسم بالعربية</label>
        <div class="col-sm-6">
            <textarea class="form-control" id="ar_name" name="ar_name" rows="4">{{$specific_cat->translate('ar')->name}}</textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for=""></label>
        <div class="text-left col-sm-10">
            <input type="submit" value="Update" id="" name="" class="btn btn-primary">
        </div>
    </div>
</form>
