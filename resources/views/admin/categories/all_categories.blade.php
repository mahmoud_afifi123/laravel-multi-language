@include('layouts.side')
<br>
<?php
$i = 1;
?>
<div class="container-fluid">
@if(count($all_categories) > 0)
    @foreach($all_categories as $cat)
        <div style="margin-left: 300px;">
            <div class="row">
                <div class="col-md-3">
                    <h3>{{ $i .'-'.$cat->slug }}</h3>
                </div>
                <div class="col-md-3">
                    <h3>{{ $cat->translate('en')->name }}</h3>
                </div>
                <div class="col-md-3">
                    <h3>{{ $cat->translate('ar')->name }}</h3>
                </div>
                <div class="col-md-3">
                    <a href="{{ url("admin/category/delete/$cat->id") }}" class="btn btn-danger">
                        Delete
                    </a>
                    <a href="{{ url("admin/category/edit/$cat->id") }}" class="btn btn-primary">
                        Edit
                    </a>

                </div>
            </div>
        </div>
        <?php $i++;?>
    @endforeach
@else
	<h1 style="margin-left:600px; margin-top:200px;">{{trans('post.not_exist')}}</h1>	
@endif
</div>
