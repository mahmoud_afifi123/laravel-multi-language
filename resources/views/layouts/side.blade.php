{{ Html::style('css/bootstrap.min.css') }}
{{ Html::style('css/datepicker3.css') }}
{{ Html::style('css/styles.css') }}
{{ Html::script('js/html5shiv.js') }}
{{ Html::script('js/lumino.glyphs.js') }}

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/client') }}"><span>Client</span></a>
            <a class="navbar-brand" href="{{ URL::to('/logout') }}"><span>logout</span></a>
        </div>
    </div>
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <ul class="nav menu">
        <li><a href="{{ URL::to('admin') }}"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg>MainPage</a></li>
        <li><a href="{{ URL::to('admin/category/create') }}" id="cat_create"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>Add Category</a></li>
        <li><a href="{{ URL::to('admin/category') }}" id="all_cats"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>All Categories</a></li>
        <li><a href="{{ URL::to('admin/post/create') }}" id=""><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>Add Post</a></li>
        <li><a href="{{ URL::to('admin/post') }}" id=""><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>All Posts</a></li>
        <li><a href="{{ URL::to('admin/user/create') }}" id=""><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>Add User</a></li>
        <li><a href="{{ URL::to('admin/user') }}" id=""><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>All Users</a></li>
    </ul>

</div>
