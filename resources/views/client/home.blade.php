@include('client.header')
<div class="container-fluid">
    <h1 align="center">{{trans('post.Home')}}</h1>
    <h2 align="center"><a class="btn btn-primary" href="{{url('/en/client/')}}">English</a>    <a class="btn btn-primary"  href="{{url('/ar/client/')}}">العربية</a></h2>
    <hr>
    @foreach($all_posts as $post)
        <div class="row">
            <div class="media @if(LaravelLocalization::setLocale() == 'ar') pull-right @endif">
                <a href="" class="@if(LaravelLocalization::setLocale() == 'ar') pull-right @else pull-left @endif"><img src="{{url("/uploads/$post->image")}}" width="200" class="media-object"></a>
                <div class="media-body">
                    <h4 class="media-heading">
                        {{trans('post.title')}}: {{ $post->translate(LaravelLocalization::setLocale())->title }}
                    </h4>
                    <h3>{{ substr($post->translate(LaravelLocalization::setLocale())->description,0,10) }}</h3>
                    <p>
                        <a class="btn btn-primary"  href="{{ url("client/details/$post->id") }}">{{trans('post.more')}}</a>
                    </p>
                </div>
            </div>

        </div>
    @endforeach
    <div>
        {{ $all_posts->links() }}
    </div>
</div>