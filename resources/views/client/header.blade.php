{{ Html::style('css/bootstrap.min.css') }}
<nav class="navbar navbar-default navbar-inverse" role="navigation">
    <div class="navbar-header">

    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <ul class="nav navbar-nav navbaphpr-right">
            <li>
                <a href="{{url("/client")}}">{{ trans('post.Home') }}</a>
            </li>
            @foreach($all_categories as $category)
                <li>
                    <a href="{{url("/client/$category->slug")}}">{{ $category->translate(LaravelLocalization::setLocale())->name }}</a>
                </li>
            @endforeach
        </ul>
    </div>

</nav>
