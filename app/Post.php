<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Translatable;

    protected $table = 'posts';
    public $translationModel = 'App\PostTranslations';
    protected $fillable = ['category_id','image'];
    public $translatedAttributes = ['title','description'];

    public function categories()
    {
        return $this->belongsTo('App\Category','category_id','id');
    }

}
