<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Translatable;

    protected $table='categories';
    public $translationModel = 'App\CategoryTranslation';
    protected $fillable = ['slug'];
    public $translatedAttributes = ['name'];

    public function posts()
    {
        return $this->hasMany('App\Post','category_id','id');
    }
}
