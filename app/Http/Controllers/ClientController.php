<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ClientController extends Controller
{
    public static $category;
    public function singleObjChecker()
    {
        if (static::$category == null)
        {
            static::$category= new Category();
        }
        return static::$category;
    }
    public function index()
    {
        $this->singleObjChecker();
        $all_posts = Post::paginate(10);
        if (Cache::get('all_categories'))
        {
            $all_categories=Cache::get('all_categories');
        }
        else {
            $all_categories = static::$category->get();
        }
        return view('client.home', compact('all_posts', 'all_categories'));
    }
    public function getCatNews($slug)
    {
        $this->singleObjChecker();
        if (Cache::get('all_categories'))
        {
            $all_categories=Cache::get('all_categories');
        }
        else {
            $all_categories = static::$category->get();
        }
        //note:i did not use eager loading with method because i want to make pagination
        $category=static::$category->where('slug',$slug)->first();
        $posts_related_to_slug = Post::where('category_id', $category->id)->paginate(10);

        return view('client.category_page', compact('posts_related_to_slug', 'all_categories','category'));
    }
    public function details($id)
    {
        $this->singleObjChecker();
        if (Cache::get('all_categories'))
        {
            $all_categories=Cache::get('all_categories');
        }
        else {
            $all_categories = static::$category->get();
        }
        $specific_post=Post::with('categories')->find($id);
        return view('client.specific_post',compact('specific_post','all_categories'));
    }
}

