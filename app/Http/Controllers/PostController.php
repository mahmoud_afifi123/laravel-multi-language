<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
    public static $post;

    //ensuring single object of post model
    public function singleObjChecker()
    {
        if (static::$post == null)
        {
            static::$post=new Post();
        }
        return static::$post;
    }
    public function index()
    {
        $this->singleObjChecker();
        if (Cache::get('all_posts'))
        {
            $all_posts=Cache::get('all_posts');
        }
        else
        {
            $all_posts=static::$post->get();
        }
        $all_posts=static::$post->get();
        return view('admin.posts.all_posts',compact('all_posts'));
    }
    public function create()
    {
        $categories=Category::get(['id','slug']);
        return view('admin.posts.add_post',compact('categories'));
    }
    public function store(Request $request)
    {
         $this->singleObjChecker();
         $category_id=$request->input('category_id');
         $en_title=$request->input('en_title');
         $ar_title=$request->input('ar_title');
         $image=$request->file('image');
         $en_description=$request->input('en_description');
         $ar_description=$request->input('ar_description');

        //image upload
        $image_name=rand(0,200).time().$image->getClientOriginalName();
        if ($image->move('uploads',$image_name))
        {
            $new_post=static::$post->create(
                [
                    'category_id'=>$category_id,
                    'title:en'=>$en_title,
                    'title:ar'=>$ar_title,
                    'image'=>$image_name,
                    'description:en'=>$en_description,
                    'description:ar'=>$ar_description
                ]
            );
            if ($new_post) {
                Cache::forever('all_posts',static::$post ->all());
                return redirect('/admin/post');
            }
        }
    }

    public function edit($id)
    {
        $categories=Category::get(['id','slug']);
        $specific_post=Post::find($id);
        return view('admin.posts.edit_post',compact('specific_post','categories'));
    }
    public function update(Request $request,$id)
    {
        $this->singleObjChecker();
        $this_post=static::$post->find($id);
        if ($request->has('$category_id')) {
            $this_post->category_id = $request->input('category_id');
        }
        if ($request->has('en_title')) {
            $this_post->{'title:en'} = $request->input('en_title');
        }
        if ($request->has('ar_title')) {
            $this_post->{'title:ar'} = $request->input('ar_title');
        }
        if ($request->hasFile('image')) {
            $image_name=rand(0,200).time().$request->file('image')->getClientOriginalName();
            if ($request->file('image')->move('uploads',$image_name)) {
                $this_post->image = $image_name;
            }
        }
        if ($request->has('en_description')) {
            $this_post->{'description:en'} = $request->input('en_description');
        }
        if ($request->has('ar_description')) {
            $this_post->{'description:ar'} = $request->input('ar_description');
        }

        if ($this_post->update())
        {
            Cache::forever('all_posts',static::$post->all());
            return redirect('/admin/post');
        }
        else
        {
            return redirect()->back();
        }
    }
    public function details($id)
    {
        $this->singleObjChecker();
        $specific_post=static::$post->with('categories')->find($id);
        return view('admin.posts.specific_post',compact('specific_post'));
    }
    public function delete($id)
    {
        $this->singleObjChecker();
        static::$post->find($id)->delete();
		Cache::forever('all_posts',static::$post->all());
        return redirect()->back();
    }

}
