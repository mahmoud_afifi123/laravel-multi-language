<?php

namespace App\Http\Controllers;

use App\language;
use App\role;
use App\Category;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public static $user;

    //ensuring single object of user model
    public function singleObjChecker()
    {
        if (static::$user == null) {
            static::$user = new User();
        }
        return static::$user;
    }

    public function index()
    {
        $this->singleObjChecker();
        $users = static::$user->get();

        return view('admin.members.all_users')->with('users', $users);
    }

    public function create()
    {
        return view('admin.members.add_user');
    }

    public function store(Request $request)
    {
        $this->singleObjChecker();
        $validate = Validator::make($request->all(), [
            'name' => 'max:255',
            'email' => 'max:255',
            'passwordinput' => 'min:6',
        ]);

        if ($validate->fails()) {

            return redirect()->back();

        } else {

            static::$user->create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('passwordinput')),
            ]);
            return redirect('/admin/user');
        }
    }


    public function edit($id)
    {
        $this->singleObjChecker();
        $specific_user = static::$user->find($id);

        return view('admin.members.edit_user', compact('specific_user'));
    }

    public function update(Request $request, $id)
    {
        $this->singleObjChecker();
        $thisUser = static::$user->find($id);
        if ($request->has('name')) {
            $thisUser->name = $request->input('name');
        }
        if ($request->has('email')) {
            $thisUser->email = $request->input('email');
        }
        if ($request->has('passwordinput')) {
            $thisUser->password = bcrypt($request->input('passwordinput'));
        }
        if ($thisUser->update()) {
            return redirect('/admin/user');
        } else {
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        $this->singleObjChecker();
        static::$user->find($id)->delete();
        return redirect()->back();
    }


}