<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CategoryController extends Controller
{
    public static $category = null;

    //ensuring single object of category model
    public function singleObjChecker()
    {
      if (static::$category == null)
      {
             static::$category=new Category();
      }
      return static::$category;
    }
    public function index()
    {
        $this->singleObjChecker();
        if (Cache::get('all_categories'))
        {
            $all_categories=Cache::get('all_categories');
        }
        else {
            $all_categories = static::$category->all();
        }
        return view('admin.categories.all_categories', compact('all_categories'));
    }

    public function create()
    {
        return view('admin.categories.add_category');
    }

    public function store(Request $request)
    {
        $this->singleObjChecker();
        $slug_name = $request->input('slug');
        $en_name = $request->input('en_name');
        $ar_name = $request->input('ar_name');
        $newCat = static::$category->create([
            'slug' => $slug_name,
            'name:en' => $en_name,
            'name:ar' => $ar_name,
        ]);
        if ($newCat) {
            Cache::forever('all_categories',static::$category ->all());
            return redirect('/admin/category');
        }


    }

    public function edit($id)
    {
        $this->singleObjChecker();
        $specific_cat = static::$category->find($id);
        return view('admin.categories.edit_category', compact('specific_cat'));
    }

    public function update(Request $request, $id)
    {
        $this->singleObjChecker();
        $this_cat=static::$category->find($id);
        if ($request->has('slug')) {
            $this_cat->name = $request->input('name');
        }
        if ($request->has('en_name')) {
            $this_cat->{'name:en'} = $request->input('en_name');
        }
        if ($request->has('ar_name')) {
            $this_cat->{'name:ar'} = $request->input('ar_name');
        }
        if ($this_cat->update())
        {
            Cache::forever('all_categories',static::$category ->all());
            return redirect('/admin/category');
        }
        else
        {
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        $this->singleObjChecker();
        static::$category->find($id)->delete();
		Cache::forever('all_categories',static::$category ->all());
        return redirect()->back();

    }
}
