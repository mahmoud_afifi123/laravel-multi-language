<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTranslations extends Model
{
    protected $table='post_translations';
    public $timestamps = false;
    protected $fillable = ['title','description'];
}